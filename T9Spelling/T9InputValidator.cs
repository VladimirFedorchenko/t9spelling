﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace T9Spelling
{
    public class T9InputValidator
    {
        public int CheckCountOfCases(string n)
        {
            if (Int32.TryParse(n, out int count)&& count >= 1 && count <= 100)
            {
                return count;
            }
            //in wrong case the method returns 0
            return 0;
        }

        public bool CheckCase(string str)
        {
            return Regex.IsMatch(str,"^[a-z ]+$");
        }
    }
}
