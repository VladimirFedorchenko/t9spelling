﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9Spelling
{
    class Program
    {
        static void Main(string[] args)
        {
            T9 t9 = new T9();
            t9.Start();
            Console.WriteLine("Press any key...");
            Console.ReadKey();
        }
    }
}