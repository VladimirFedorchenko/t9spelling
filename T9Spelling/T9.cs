﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9Spelling
{
    public class T9
    {
        T9Converter converter;
        T9InputValidator inputValidator;
        List<string> casesList;
        int casesCount;

        public T9()
        {
            converter = new T9Converter();
            inputValidator = new T9InputValidator();
        }

        public void Start()
        {
            casesList = new List<string>();
            InputCasesCount();
            InputCasesList();
            ConvertCases();
        }

        private void ConvertCases()
        {
            for (int i = 0; i < casesCount; i++)
            {
                Console.WriteLine("Case #{0}: {1}", i + 1, converter.Convert(casesList[i]));
            }
        }

        private void InputCasesList()
        {
            for (int i = 0; i < casesCount; i++)
            {
                Console.WriteLine("Input case {0}(only lowercase and space characters)", i + 1);
                string str = Console.ReadLine().ToLower();
                //if input is wrong decrement i and repeat the cycle
                if (!inputValidator.CheckCase(str))
                {
                    Console.WriteLine("Wrong input!");
                    i--;
                }
                else
                {
                    casesList.Add(str);
                }
            }
        }

        private void InputCasesCount()
        {
            while (true)
            {
                Console.WriteLine("Input count of cases N(1<=N<=100)");
                casesCount = inputValidator.CheckCountOfCases(Console.ReadLine());
                if (casesCount == 0)
                {
                    Console.WriteLine("Wrong input!");
                }
                else
                {
                    return;
                }
            }
        }
    }
}
