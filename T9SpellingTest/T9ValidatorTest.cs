﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9Spelling;

namespace T9SpellingTest
{
    [TestClass]
    public class T9ValidatorTest
    {
        [TestMethod]
        public void CheckCaseRightTestMethod()
        {
            T9InputValidator inputValidator = new T9InputValidator();
            bool isRight = inputValidator.CheckCase("hello world");
            Assert.AreEqual(true, isRight);
        }

        [TestMethod]
        public void CheckCaseWrongTestMethod()
        {
            T9InputValidator inputValidator = new T9InputValidator();
            bool isRight = inputValidator.CheckCase("Hello world");
            Assert.AreEqual(false, isRight);
        }

        [TestMethod]
        public void CheckCountOfCasesRightTestMethod()
        {
            T9InputValidator inputValidator = new T9InputValidator();
            int count = inputValidator.CheckCountOfCases("10");
            Assert.AreEqual(10, count);
        }

        [TestMethod]
        public void CheckCountOfCasesWrongTestMethod()
        {
            T9InputValidator inputValidator = new T9InputValidator();
            int count = inputValidator.CheckCountOfCases("110");
            Assert.AreEqual(0, count);
        }
    }
}
