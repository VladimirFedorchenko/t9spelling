﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using T9Spelling;

namespace T9SpellingTest
{
    [TestClass]
    public class T9ConverterTest
    {
        [TestMethod]
        public void HelloWorldConverterTestMethod()
        {
            T9Converter converter = new T9Converter();
            string outputStr = converter.Convert("hello world");
            Assert.AreEqual("4433555 555666096667775553", outputStr);
        }

        [TestMethod]
        public void FooBarConverterTestMethod()
        {
            T9Converter converter = new T9Converter();
            string outputStr = converter.Convert("foo  bar");
            Assert.AreEqual("333666 6660 022 2777", outputStr);
        }

        [TestMethod]
        public void YesConverterTestMethod()
        {
            T9Converter converter = new T9Converter();
            string outputStr = converter.Convert("yes");
            Assert.AreEqual("999337777", outputStr);
        }

        [TestMethod]
        public void HiConverterTestMethod()
        {
            T9Converter converter = new T9Converter();
            string outputStr = converter.Convert("hi");
            Assert.AreEqual("44 444", outputStr);
        }
    }
}
